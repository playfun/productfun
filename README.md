# README #

This is a minimal in memory static DB impl for a graphql server. This is purely a POC to solve the problems of bloated apis.

### What is this repository for? ###

* Demo and discuss pros-cons of graphql over rest apis
* An attempt to solve issues of over-fetching and under-fetching
* Apart from the above common use case, this POC also attempts to show data aggregation from multiple sources in an efficient manner.

### How do I get set up? ###

A simple sbt run should get you set up.
Look at the schema at `/schema` endpoint
Once schema is available you can try a query through `/graphql` POST endpoint by sending json request like
```
{
  "query": "{Products(Key: \"prod\", BrandArg: \"brandx\"){TotalCount, Details{Name, Description, Brand{Name}, Inventory{Details{LotSize}}}}}"
}
```
For legibility, I'll just be referencing the grahql query from now on, which looks like this.

```
{
  Products(Key: "prod", BrandArg: "brandx"){
    TotalCount,
    Details{
      Name,
      Description,
      Brand{
        Name
      },
      Inventory{
        Details{
          LotSize
        }
      }
    }
  }
}
```

Once you have the query running, go ahead and add Price to your query like so

```
{
  Products(Key: "prod", BrandArg: "brandx"){
    TotalCount,
    Details{
      Name,
      Description,
      Brand{
        Name
      },
      Inventory{
        Details{
          LotSize
        }
      },
      Price
    }
  }
}
```

Don't like the Price being shown in CAD because the app serves US market too.
No worries just ask your client-app to send in a currency argument for Price type

```
Price(currency: USD)
```

Now your front end serves pricing info in USD too.

No need to add market specifc APIs, form-factor specific APIs and the app devs are happy as they just gets what they want.
No over-fetching and no multiple round trips due to under-fetching.

### Deployment Instructions ###

build.sbt uses sbt-native-packager to build a docker image.
productfun/productfunchart houses helmcharts and associated manifests to deploy a release to kubernetes.

### Contribution guidelines ###
N/A

### Who do I talk to? ###

* Mayank Srivastava (sriv.mayank@gmail.com)