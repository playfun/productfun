package com.playground.schema

import java.util.UUID

import com.google.inject.Inject
import com.google.inject.name.Named
import com.playground.model._
import com.playground.repository.inventory.InventoryRepository
import com.playground.repository.product.ProductRepository
import com.playground.repository.sales.PricingRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SchemaContext @Inject()(@Named("staticProductRepository") productRepository: ProductRepository,
                              @Named("staticInventoryRepository") inventoryRepository: InventoryRepository,
                              @Named("staticPricingRepository") pricingRepository: PricingRepository) {

  def product(key: String, brand: Option[String] = None): Future[ProductResult] =
    for {
      products <- productRepository.getProductsByNameLike(key, brand)
      size = products.size
    } yield ProductResult(size, products)

  def inventory(id: UUID): Future[Option[ProductInventory]] = inventoryRepository.getInventoryForProductId(id)

  def price(id: UUID, currency: Option[String]): Future[Option[String]] = pricingRepository.getProductPriceById(id).map(p => (p, currency) match {
    case (Some(p), Some(c)) if c.toLowerCase == "usd" => Some(s"USD ${p * 0.75}")
    case (Some(p), _) => Some(s"CAD $p")
    case _ => None
  })


}
