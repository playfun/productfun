package com.playground.schema

import com.playground.model._
import sangria.schema._

object SchemaDefinition {

  val Key = Argument("Key", StringType, description = "key word to search contact")
  val BrandArg = Argument("BrandArg", OptionInputType(StringType), description = "filter by brand")

  val Currency = Argument("Currency", OptionInputType(StringType), description = "chose currency type")

  val BrandType = ObjectType(
    "Brand",
    "Brand Type Description",
    fields[SchemaContext, Brand](
      Field("Id", StringType, Some("brand Id"), resolve = _.value.id.toString),
      Field("Name", OptionType(StringType), Some("brand name"), resolve = _.value.name)
    )
  )

  val StoreInfoType = ObjectType(
    "StoreInfo",
    "Store Info Description",
    fields[SchemaContext, StoreInfo](
      Field("Id", StringType, Some("store Id"), resolve = _.value.id.toString),
      Field("WarehouseLoc", StringType, Some("warehouse location"), resolve = _.value.warehouseLocation)
    )
  )

  val QtyInStoreType = ObjectType(
    "QtyInStore",
    "Store Quantity of product",
    fields[SchemaContext, QtyInStore](
      Field("Store", StoreInfoType, Some("Store info"), resolve = _.value.store),
      Field("LotSize", LongType, Some("lot size"), resolve = _.value.lotSize)
    )
  )

  val ProductInventoryType = ObjectType(
    "ProductInventory",
    "Product Inventory Description",
    fields[SchemaContext, ProductInventory](
      Field("ProductId", StringType, Some("product Id"), resolve = _.value.productId.toString),
      Field("BatchNumber", StringType, Some("batch number"), resolve = _.value.batchNumber),
      Field("Details", ListType(QtyInStoreType), Some("inventory details"), resolve = _.value.details)
    )
  )

  val ProductType = ObjectType(
    "Product",
    "Product Type Description",
    fields[SchemaContext, Product](
      Field("Id", StringType, Some("product Id"), resolve = _.value.id.toString),
      Field("ModelNumber", OptionType(StringType), Some("product Id"), resolve = _.value.modelNumber),
      Field("Name", OptionType(StringType), Some("product Id"), resolve = _.value.name),
      Field("Description", OptionType(StringType), Some("product Id"), resolve = _.value.description),
      Field("Brand", OptionType(BrandType), Some("product Id"), resolve = _.value.brand),
      Field("Price", OptionType(StringType), Some("product price"), arguments = Currency :: Nil, resolve = ctx => ctx.ctx.price(ctx.value.id, ctx.arg(Currency))),
      Field("Inventory", OptionType(ProductInventoryType), Some("inventory"), resolve = ctx => ctx.ctx.inventory(ctx.value.id))
    )
  )

  val ProductResultType = ObjectType(
    "ProductResult",
    "Product Result Description",
    fields[SchemaContext, ProductResult](
      Field("TotalCount", IntType, Some("total count"), resolve = _.value.totalCount),
      Field("Details", ListType(ProductType), Some("product list"), resolve = _.value.products)
    )
  )

  val Query = ObjectType(
    "Query",
    fields[SchemaContext, Unit](
      Field("Products", ProductResultType, Some("Query for products"), arguments = Key :: BrandArg :: Nil,
        resolve = ctx => ctx.ctx.product(ctx.arg(Key), ctx.arg(BrandArg)))
    )
  )

  val ProductSchema = Schema(Query)
}
