package com.playground.model

import java.util.UUID

case class Product(id: UUID,
                   modelNumber: String,
                   name: String,
                   description: String,
                   brand: Brand)

case class Brand(id: UUID,
                 name: String)
