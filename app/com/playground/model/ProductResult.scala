package com.playground.model

case class ProductResult(totalCount: Int,
                         products: Seq[Product])
