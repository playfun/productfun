package com.playground.model

import java.util.UUID

case class ProductInventory(productId: UUID,
                            batchNumber: String,
                            details: Seq[QtyInStore])

case class StoreInfo(id: UUID,
                     warehouseLocation: String)

case class QtyInStore(store: StoreInfo,
                      lotSize: Long)
