package com.playground.infrastructure.play.modules

import com.google.inject.AbstractModule
import com.google.inject.name.Names
import com.playground.repository.inventory._
import com.playground.repository.product._
import com.playground.repository.sales._

class RepositoriesModule extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[ProductRepository]).annotatedWith(Names.named("staticProductRepository")).to(classOf[ProductRepositoryStaticDBImpl])
    bind(classOf[InventoryRepository]).annotatedWith(Names.named("staticInventoryRepository")).to(classOf[InventoryRepositoryStaticDBImpl])
    bind(classOf[PricingRepository]).annotatedWith(Names.named("staticPricingRepository")).to(classOf[PricingRepositoryStaticDBImpl])
  }

}
