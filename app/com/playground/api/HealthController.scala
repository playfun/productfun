package com.playground.api

import javax.inject._
import play.api.mvc._

class HealthController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def health = Action { implicit request => Ok }
}
