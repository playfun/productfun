package com.playground.api

import java.util.UUID

import com.google.inject.Inject
import com.google.inject.name.Named
import com.playground.repository.inventory.InventoryRepository
import com.playground.repository.product.ProductRepository
import com.playground.schema.{SchemaContext, SchemaDefinition}
import play.api.libs.json.{JsObject, JsString, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import sangria.execution.{ErrorWithResolver, Executor, QueryAnalysisError}
import sangria.marshalling.playJson._
import sangria.parser.{QueryParser, SyntaxError}
import sangria.renderer.SchemaRenderer

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class ProductController @Inject()(cc: ControllerComponents,
                                  schemaContext: SchemaContext,
                                  @Named("staticProductRepository") productRepository: ProductRepository,
                                  @Named("staticInventoryRepository") inventoryRepository: InventoryRepository)
                                  (implicit val ec: ExecutionContext) extends AbstractController(cc: ControllerComponents)
  with JsonConversionImplicits {

  def graphql(query: String, variables: Option[String], operation: Option[String]) = Action.async {
    executeQuery(query, variables.map(parseVariables(_)), operation)
  }

  def graphqlPost = Action.async {
    implicit request =>
      request.body.asJson match {
        case Some(json) =>
          val query = (json \ "query").as[String]
          val operation = (json \ "operationName").asOpt[String]
          val variables = (json \ "variables").toOption.flatMap {
            case JsString(vars) => Some(parseVariables(vars))
            case obj: JsObject => Some(obj)
            case _ => None
          }
          executeQuery(query, variables, operation)
        case _ => Future.successful(BadRequest("Invalid JSON"))
      }
  }

  private def parseVariables(variables: String) =
    if (variables.trim == "" || variables.trim == "null") Json.obj() else Json.parse(variables).as[JsObject]

  private def executeQuery(query: String,
                           variables: Option[JsObject],
                           operation: Option[String]) =

    QueryParser.parse(query) match {
      case Success(queryAst) =>
        Executor.execute(
          SchemaDefinition.ProductSchema,
          queryAst,
          userContext = schemaContext,
          operationName = operation,
          variables = variables getOrElse Json.obj()
        ).map(Ok(_)).recover {
          case error: QueryAnalysisError => BadRequest(error.resolveError)
          case error: ErrorWithResolver => InternalServerError(error.resolveError)
        }
      case Failure(error: SyntaxError) =>
        Future.successful(BadRequest("malformed_query"))
      case Failure(error) =>
        throw error
    }

  def schema = Action {
    implicit req =>
      Ok(SchemaRenderer.renderSchema(SchemaDefinition.ProductSchema))
  }

  @deprecated
  def product(id: UUID) = Action.async {
    implicit req => productRepository.getProductById(id).map(res => Ok(Json.toJson(res)))
  }

  @deprecated
  def inventory(id: UUID) = Action.async {
    implicit req => inventoryRepository.getInventoryForProductId(id).map(res => Ok(Json.toJson(res)))
  }

}
