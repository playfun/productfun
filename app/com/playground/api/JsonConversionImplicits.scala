package com.playground.api

import ai.x.play.json.Encoders.encoder
import ai.x.play.json.Jsonx
import com.playground.model._

trait JsonConversionImplicits {

  implicit lazy val brandFormats = Jsonx.formatCaseClass[Brand]
  implicit lazy val productWrites = Jsonx.formatCaseClass[Product]

  implicit lazy val storeInfoWrites = Jsonx.formatCaseClass[StoreInfo]
  implicit lazy val qtyInStoreWrites = Jsonx.formatCaseClass[QtyInStore]
  implicit lazy val productInventoryWrites = Jsonx.formatCaseClass[ProductInventory]

}
