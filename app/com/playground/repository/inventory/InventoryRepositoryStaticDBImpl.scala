package com.playground.repository.inventory

import java.util.UUID

import com.playground.model._
import play.api.Logger

import scala.concurrent.Future

class InventoryRepositoryStaticDBImpl extends InventoryRepository {

  val logger: Logger = Logger(this.getClass())

  val stores = Seq(StoreInfo(UUID.fromString("6848a883-fa68-4e74-a4c8-ef6e85886fa2"), "Toronto"),
    StoreInfo(UUID.fromString("34283c30-3ca0-4ec8-b157-b35e30ceec38"), "Oakville"),
    StoreInfo(UUID.fromString("0f99e463-85da-40db-b777-380de61b5a5b"), "Mississauga")
  )

  val inventories = Seq(ProductInventory(UUID.fromString("166ad8c9-130d-4ae5-8b45-5841dafff6ef"), "123-afg", Seq(QtyInStore(stores.find(_.warehouseLocation == "Toronto").get, 30), QtyInStore(stores.find(_.warehouseLocation == "Toronto").get, 15))),
    ProductInventory(UUID.fromString("2e548f30-aa99-481c-baf2-3fc3155a5848"), "123-afg", Seq(QtyInStore(stores.find(_.warehouseLocation == "Oakville").get, 20))),
    ProductInventory(UUID.fromString("f2baa500-8b1a-40c7-9d16-3e3a5cd595ec"), "123-afg", Seq(QtyInStore(stores.find(_.warehouseLocation == "Toronto").get, 30), QtyInStore(stores.find(_.warehouseLocation == "Oakville").get, 18))),
    ProductInventory(UUID.fromString("2685a3a5-2c07-4cfc-a471-6cde67ed9963"), "123-afg", Seq(QtyInStore(stores.find(_.warehouseLocation == "Mississauga").get, 40))),
    ProductInventory(UUID.fromString("9294d760-0f1d-4466-b377-0bca5256085d"), "123-afg", Seq(QtyInStore(stores.find(_.warehouseLocation == "Mississauga").get, 9))),
    ProductInventory(UUID.fromString("36457c90-9a1f-43de-99b8-3de78d9c6f1b"), "123-afg", Seq(QtyInStore(stores.find(_.warehouseLocation == "Toronto").get, 15), QtyInStore(stores.find(_.warehouseLocation == "Oakville").get, 12), QtyInStore(stores.find(_.warehouseLocation == "Mississauga").get, 18)))
  )

  override def getInventoryForProductId(id: UUID): Future[Option[ProductInventory]] = {
    System.out.println("Inventory DB hit")
    Future.successful(inventories.find(_.productId == id))
  }
}
