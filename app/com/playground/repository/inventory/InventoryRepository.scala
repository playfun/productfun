package com.playground.repository.inventory

import java.util.UUID

import com.playground.model._

import scala.concurrent.Future

trait InventoryRepository {

  def getInventoryForProductId(id: UUID): Future[Option[ProductInventory]]
}
