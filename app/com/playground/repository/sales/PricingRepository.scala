package com.playground.repository.sales

import java.util.UUID

import scala.concurrent.Future

trait PricingRepository {

  def getProductPriceById(id: UUID): Future[Option[Double]]
}
