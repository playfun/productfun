package com.playground.repository.sales
import java.util.UUID

import scala.concurrent.Future

class PricingRepositoryStaticDBImpl extends PricingRepository {

  val productPrice = Seq((UUID.fromString("166ad8c9-130d-4ae5-8b45-5841dafff6ef"), 10.0),
    (UUID.fromString("2e548f30-aa99-481c-baf2-3fc3155a5848"), 20.0),
    (UUID.fromString("f2baa500-8b1a-40c7-9d16-3e3a5cd595ec"), 15.0),
    (UUID.fromString("2685a3a5-2c07-4cfc-a471-6cde67ed9963"), 8.0),
    (UUID.fromString("9294d760-0f1d-4466-b377-0bca5256085d"), 10.0),
    (UUID.fromString("36457c90-9a1f-43de-99b8-3de78d9c6f1b"), 15.0))

  override def getProductPriceById(id: UUID): Future[Option[Double]] = Future.successful(productPrice.find(_._1 == id).map(_._2))
}
