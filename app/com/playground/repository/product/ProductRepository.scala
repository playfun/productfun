package com.playground.repository.product

import java.util.UUID

import com.playground.model._

import scala.concurrent.Future

trait ProductRepository {

  def getProductById(id: UUID): Future[Option[Product]]
//  def getProductsByBrandId(id: UUID): Future[Seq[Product]]
  def getProductsByNameLike(key: String, brand: Option[String] = None): Future[Seq[Product]]

  def getBrandsById(id: UUID): Future[Option[Brand]]
}
