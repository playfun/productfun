package com.playground.repository.product

import java.util.UUID

import com.playground.model._

import scala.concurrent.Future

class ProductRepositoryStaticDBImpl extends ProductRepository {

  val brands = Seq(Brand(UUID.fromString("f743044a-3e5c-4665-b790-669018bb3ff9"), "BrandX"),
    Brand(UUID.fromString("dd8d6dbe-9893-4e7e-821b-9c824f895376"), "BrandY"),
    Brand(UUID.fromString("1281fbae-ecc4-47ac-899a-6d405311b178"), "BrandZ")
  )

  val products = Seq(Product(UUID.fromString("166ad8c9-130d-4ae5-8b45-5841dafff6ef"), "zxcvbn", "ProductXA", "description for BrandX's product XA", brands.find(_.name == "BrandX").get),
    Product(UUID.fromString("2e548f30-aa99-481c-baf2-3fc3155a5848"), "qwerty", "ProductXB", "description for BrandX's product XB", brands.find(_.name == "BrandX").get),
    Product(UUID.fromString("f2baa500-8b1a-40c7-9d16-3e3a5cd595ec"), "qazxsw", "ProductYC", "description for BrandY's product YC", brands.find(_.name == "BrandY").get),
    Product(UUID.fromString("2685a3a5-2c07-4cfc-a471-6cde67ed9963"), "wsxzaq", "ProductYD", "description for BrandY's product YD", brands.find(_.name == "BrandY").get),
    Product(UUID.fromString("9294d760-0f1d-4466-b377-0bca5256085d"), "edcvfr", "ProductZE", "description for BrandZ's product ZE", brands.find(_.name == "BrandZ").get),
    Product(UUID.fromString("36457c90-9a1f-43de-99b8-3de78d9c6f1b"), "rfvcde", "ProductZF", "description for BrandZ's product ZF", brands.find(_.name == "BrandZ").get)
  )

  override def getProductById(id: UUID): Future[Option[Product]] = Future.successful(products.find(_.id == id))

//  override def getProductsByBrandId(id: UUID): Future[Seq[Product]] = Future.successful(products.filter(_.brand.id == id))

  override def getProductsByNameLike(key: String, brandKey: Option[String]): Future[Seq[Product]] = Future.successful {
    brandKey match {
      case Some(bk) => products.filter(p => p.name.toLowerCase.startsWith(key.toLowerCase) && p.brand.name.toLowerCase.startsWith(bk.toLowerCase))
      case _ => products.filter(_.name.toLowerCase.startsWith(key.toLowerCase))
    }
  }

  override def getBrandsById(id: UUID): Future[Option[Brand]] = Future.successful(brands.find(_.id == id))
}
