name := """productfun"""

organization := "com.playground"

version := "2.0-SNAPSHOT"

scalaVersion := "2.13.3"

val deps = Seq(
  guice,
  "ai.x" %% "play-json-extensions" % "0.42.0",
  "com.typesafe.slick" %% "slick" % "3.3.2",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
  "org.sangria-graphql" %% "sangria" % "2.0.0",
  "org.sangria-graphql" %% "sangria-play-json" % "2.0.0"
)

lazy val root = (project in file(".")).
  settings(libraryDependencies ++= deps).
  enablePlugins(PlayScala, SbtNativePackager)

javaOptions in Universal += "-Dplay.server.pidfile.path=/dev/null"

javaOptions in Universal += "-Dfile.encoding=UTF8"

javaOptions in Universal += "-Dhttp.port=9000"

dockerRepository := Some(s"docker.artifactory.local/${organization.value}")

maintainer in Docker := "sriv.mayank@gmail.com"

daemonUser in Docker := "root"

dockerBuildOptions ++= Seq("--network", "host")

dockerBaseImage := "adoptopenjdk/openjdk11"

dockerExposedPorts := Seq(9000)